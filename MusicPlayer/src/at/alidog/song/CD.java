package at.alidog.song;

import java.util.List;

public class CD {
	private List<Integer> mySongs;

	public List<Integer> getMySongs() {
		return mySongs;
	}

	public void setMySongs(List<Integer> mySongs) {
		this.mySongs = mySongs;
	}
}
