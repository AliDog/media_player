package at.alidog.song;

import at.alidog.player.Playable;

public class Song implements Playable {

int songID;	
String title;
float length;

@Override
public void play() {
	System.out.println("The song " + title + " is now playing");
	
}
@Override
public void stop() {
	System.out.println("The song " + title + " is not playing anymore");
	
}
@Override
public void pause() {
	System.out.println("The song " + title + " is now on pause");
	
}

}
