package at.alidog.movie;

import at.alidog.player.Playable;

public class Movie implements Playable{

int MovieID;
String title;

	
	@Override
	public void play() {
		System.out.println("The movie " + title + " is now playing");
		
	}

	@Override
	public void stop() {
		System.out.println("The movie " + title + " is not playing anymore");
		
	}

	@Override
	public void pause() {
		System.out.println("The movie " + title + " is now on pause");
		
	}

}
