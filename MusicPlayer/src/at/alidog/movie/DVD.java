package at.alidog.movie;

import java.util.List;

public class DVD {
	
	
	private List<Integer> myMovies;

	public List<Integer> getMyMovies() {
		return myMovies;
	}

	public void setMyMovies(List<Integer> myMovies) {
		this.myMovies = myMovies;
	}
}
